# swagger_client.BeneficiariesApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_beneficiaries**](BeneficiariesApi.md#get_beneficiaries) | **GET** /beneficiaries | search beneficiary bank accounts
[**get_beneficiary**](BeneficiariesApi.md#get_beneficiary) | **GET** /beneficiaries/{id} | get a beneficiary bank account
[**post_beneficiary**](BeneficiariesApi.md#post_beneficiary) | **POST** /beneficiaries | create a beneficiary
[**put_beneficiary**](BeneficiariesApi.md#put_beneficiary) | **PUT** /beneficiaries/{id} | edit a beneficiary


# **get_beneficiaries**
> object get_beneficiaries(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, fields=fields, filter=filter, id=id, user_id=user_id, iban=iban, bic=bic, nick_name=nick_name, name=name, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order)

search beneficiary bank accounts

Get beneficiary bank accounts that match search criteria.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BeneficiariesApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
fields = ['fields_example'] # list[str] | List of the object's properties you want to pick up. (optional)
filter = 'filter_example' # str | You can filter the API response by using filter(s). Filterable fields are :  - id  - tag  - userId  - isActive (1 for yes, 0 for no)  - nickName  - address  - iban (encrypted IBAN)  - IbanInClear  - bic  - sepaCreditorIdentifier  - usableForSct  - createdDate  - modifiedDate  Filters should be separated by a \";\". Example for 3 filters : FILTER1;FILTER2;FILTER3. A single filter has the following syntax : \"fieldName criterion expression\". Where :  - fieldName : the name of a filterable field of this object.   - expression : the values to be included or excluded (see the table below for more information)   - criterion : a filter criterion.  Here are the possible values for criterion :   | Criteria |         Description    |                   Type                   | Expression Example |  |----------|------------------------|------------------------------------------|--------------------|  |     >    |      greater than      |            alphanumeric string           |         100        |  |    >=    | greater or equal than  |            alphanumeric string           |         100        |  |     <    |        less than       |            alphanumeric string           |         100        |  |    <=    |   less or equal than   |            alphanumeric string           |         100        |  |    !=    |      not equal to      |            alphanumeric string           |         100        |  |   like   |          like          |            alphanumeric string           |         100        |  |    in    |           in           | alphanumeric strings separated by commas |      100,30,25     |  |    ==    |         equals         |            alphanumeric string           |         100        |  (optional)
id = 56 # int | Beneficiary bank account id. (optional)
user_id = 56 # int | Owner user's id. (deprecated, you must use the parameter filter) (optional)
iban = 'iban_example' # str | Beneficiary bank account IBAN. (deprecated, you must use the parameter filter) (optional)
bic = 'bic_example' # str | Beneficiary bank account BIC. (deprecated, you must use the parameter filter) (optional)
nick_name = 'nick_name_example' # str | Beneficiary bank account's nick name. (deprecated, you must use the parameter filter) (optional)
name = 'name_example' # str | Beneficiary bank account owner's name. (deprecated, you must use the parameter filter) (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)

try:
    # search beneficiary bank accounts
    api_response = api_instance.get_beneficiaries(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, fields=fields, filter=filter, id=id, user_id=user_id, iban=iban, bic=bic, nick_name=nick_name, name=name, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BeneficiariesApi->get_beneficiaries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **fields** | [**list[str]**](str.md)| List of the object&#39;s properties you want to pick up. | [optional] 
 **filter** | **str**| You can filter the API response by using filter(s). Filterable fields are :  - id  - tag  - userId  - isActive (1 for yes, 0 for no)  - nickName  - address  - iban (encrypted IBAN)  - IbanInClear  - bic  - sepaCreditorIdentifier  - usableForSct  - createdDate  - modifiedDate  Filters should be separated by a \&quot;;\&quot;. Example for 3 filters : FILTER1;FILTER2;FILTER3. A single filter has the following syntax : \&quot;fieldName criterion expression\&quot;. Where :  - fieldName : the name of a filterable field of this object.   - expression : the values to be included or excluded (see the table below for more information)   - criterion : a filter criterion.  Here are the possible values for criterion :   | Criteria |         Description    |                   Type                   | Expression Example |  |----------|------------------------|------------------------------------------|--------------------|  |     &gt;    |      greater than      |            alphanumeric string           |         100        |  |    &gt;&#x3D;    | greater or equal than  |            alphanumeric string           |         100        |  |     &lt;    |        less than       |            alphanumeric string           |         100        |  |    &lt;&#x3D;    |   less or equal than   |            alphanumeric string           |         100        |  |    !&#x3D;    |      not equal to      |            alphanumeric string           |         100        |  |   like   |          like          |            alphanumeric string           |         100        |  |    in    |           in           | alphanumeric strings separated by commas |      100,30,25     |  |    &#x3D;&#x3D;    |         equals         |            alphanumeric string           |         100        |  | [optional] 
 **id** | **int**| Beneficiary bank account id. | [optional] 
 **user_id** | **int**| Owner user&#39;s id. (deprecated, you must use the parameter filter) | [optional] 
 **iban** | **str**| Beneficiary bank account IBAN. (deprecated, you must use the parameter filter) | [optional] 
 **bic** | **str**| Beneficiary bank account BIC. (deprecated, you must use the parameter filter) | [optional] 
 **nick_name** | **str**| Beneficiary bank account&#39;s nick name. (deprecated, you must use the parameter filter) | [optional] 
 **name** | **str**| Beneficiary bank account owner&#39;s name. (deprecated, you must use the parameter filter) | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_beneficiary**
> object get_beneficiary(id, fields=fields)

get a beneficiary bank account

Get a beneficiary bank account from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BeneficiariesApi(swagger_client.ApiClient(configuration))
id = 789 # int | Beneficiary Bank Accounts internal id.
fields = ['fields_example'] # list[str] | List of the object's properties you want to pick up. (optional)

try:
    # get a beneficiary bank account
    api_response = api_instance.get_beneficiary(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BeneficiariesApi->get_beneficiary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Beneficiary Bank Accounts internal id. | 
 **fields** | [**list[str]**](str.md)| List of the object&#39;s properties you want to pick up. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_beneficiary**
> object post_beneficiary(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, sdd_b2b_whitelist=sdd_b2b_whitelist, sdd_core_blacklist=sdd_core_blacklist, body=body)

create a beneficiary

Create a new benefeciary bank account in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BeneficiariesApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
sdd_b2b_whitelist = ['sdd_b2b_whitelist_example'] # list[str] | Each unique mandate reference, with its frequency type, must be explicitely allowed when doing B2B Direct Debit. **The entry is not case sensitive.** Furthermore, a mandate not used during more than 36 months will be automatically rejected even if in the white list.  (optional)
sdd_core_blacklist = ['sdd_core_blacklist_example'] # list[str] | Core Direct Debit are accepted by default. If a Core mandate is to be refused on reception, its UMR has to be added to this list. **The entry is not case sensitive.** If wild char * (star) is used instead of a UMR, all Direct Debit from this beneficiary will be refused.  (optional)
body = swagger_client.Body() # Body |  (optional)

try:
    # create a beneficiary
    api_response = api_instance.post_beneficiary(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, sdd_b2b_whitelist=sdd_b2b_whitelist, sdd_core_blacklist=sdd_core_blacklist, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BeneficiariesApi->post_beneficiary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **sdd_b2b_whitelist** | [**list[str]**](str.md)| Each unique mandate reference, with its frequency type, must be explicitely allowed when doing B2B Direct Debit. **The entry is not case sensitive.** Furthermore, a mandate not used during more than 36 months will be automatically rejected even if in the white list.  | [optional] 
 **sdd_core_blacklist** | [**list[str]**](str.md)| Core Direct Debit are accepted by default. If a Core mandate is to be refused on reception, its UMR has to be added to this list. **The entry is not case sensitive.** If wild char * (star) is used instead of a UMR, all Direct Debit from this beneficiary will be refused.  | [optional] 
 **body** | [**Body**](.md)|  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_beneficiary**
> object put_beneficiary(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, sdd_b2b_whitelist=sdd_b2b_whitelist, sdd_core_blacklist=sdd_core_blacklist, body=body)

edit a beneficiary

Edit a benefeciary bank account in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BeneficiariesApi(swagger_client.ApiClient(configuration))
id = 56 # int | Beneficiary's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
sdd_b2b_whitelist = ['sdd_b2b_whitelist_example'] # list[str] | Each unique mandate reference, with its frequency type, must be explicitely allowed when doing B2B Direct Debit. **The entry is not case sensitive.** Furthermore, a mandate not used during more than 36 months will be automatically rejected even if in the white list.  (optional)
sdd_core_blacklist = ['sdd_core_blacklist_example'] # list[str] | Core Direct Debit are accepted by default. If a Core mandate is to be refused on reception, its UMR has to be added to this list. **The entry is not case sensitive.** If wild char * (star) is used instead of a UMR, all Direct Debit from this beneficiary will be refused.  (optional)
body = swagger_client.Body() # Body |  (optional)

try:
    # edit a beneficiary
    api_response = api_instance.put_beneficiary(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, sdd_b2b_whitelist=sdd_b2b_whitelist, sdd_core_blacklist=sdd_core_blacklist, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BeneficiariesApi->put_beneficiary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Beneficiary&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **sdd_b2b_whitelist** | [**list[str]**](str.md)| Each unique mandate reference, with its frequency type, must be explicitely allowed when doing B2B Direct Debit. **The entry is not case sensitive.** Furthermore, a mandate not used during more than 36 months will be automatically rejected even if in the white list.  | [optional] 
 **sdd_core_blacklist** | [**list[str]**](str.md)| Core Direct Debit are accepted by default. If a Core mandate is to be refused on reception, its UMR has to be added to this list. **The entry is not case sensitive.** If wild char * (star) is used instead of a UMR, all Direct Debit from this beneficiary will be refused.  | [optional] 
 **body** | [**Body**](.md)|  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# swagger_client.CardApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cardimages_get**](CardApi.md#cardimages_get) | **GET** /cardimages | get a card image
[**cards_create_virtual_post**](CardApi.md#cards_create_virtual_post) | **POST** /cards/CreateVirtual | create a virtual card
[**cards_get**](CardApi.md#cards_get) | **GET** /cards | search cards
[**cards_id_activate_put**](CardApi.md#cards_id_activate_put) | **PUT** /cards/{id}/Activate/ | activate a card
[**cards_id_change_pin_put**](CardApi.md#cards_id_change_pin_put) | **PUT** /cards/{id}/ChangePIN/ | change card&#39;s PIN
[**cards_id_convert_virtual_put**](CardApi.md#cards_id_convert_virtual_put) | **PUT** /cards/{id}/ConvertVirtual/ | convert card to virtual
[**cards_id_get**](CardApi.md#cards_id_get) | **GET** /cards/{id} | get a card
[**cards_id_limits_put**](CardApi.md#cards_id_limits_put) | **PUT** /cards/{id}/Limits/ | update card&#39;s limits
[**cards_id_lock_unlock_put**](CardApi.md#cards_id_lock_unlock_put) | **PUT** /cards/{id}/LockUnlock/ | update card&#39;s blocking status
[**cards_id_options_put**](CardApi.md#cards_id_options_put) | **PUT** /cards/{id}/Options/ | update card&#39;s options
[**cards_id_put**](CardApi.md#cards_id_put) | **PUT** /cards/{id} | update card informations
[**cards_id_regenerate_put**](CardApi.md#cards_id_regenerate_put) | **PUT** /cards/{id}/Regenerate/ | regenerate card
[**cards_id_set_pin_put**](CardApi.md#cards_id_set_pin_put) | **PUT** /cards/{id}/setPIN/ | set card&#39;s PIN
[**cards_id_unblock_pin_put**](CardApi.md#cards_id_unblock_pin_put) | **PUT** /cards/{id}/UnblockPIN/ | unblock card&#39;s PIN
[**cards_register3_ds_post**](CardApi.md#cards_register3_ds_post) | **POST** /cards/Register3DS | Register 3D secure
[**cards_request_physical_post**](CardApi.md#cards_request_physical_post) | **POST** /cards/RequestPhysical | create a physical card


# **cardimages_get**
> object cardimages_get(card_id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

get a card image

Return virtual card's image

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
card_id = 56 # int | Vitual card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # get a card image
    api_response = api_instance.cardimages_get(card_id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cardimages_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **card_id** | **int**| Vitual card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_create_virtual_post**
> object cards_create_virtual_post(user_id, wallet_id, perms_group, card_print, access_signature=access_signature, card_tag=card_tag, batch_delivery_id=batch_delivery_id, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, pin=pin, anonymous=anonymous, send_to_parent=send_to_parent, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, emboss_legal_name=emboss_legal_name, logo_id=logo_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, package_id=package_id)

create a virtual card

Create a new virtual card. To use a card you will need to activate it (/cards/{id}/Activate/).

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
user_id = 56 # int | User's id of cardholder
wallet_id = 56 # int | Wallet's Id in which to create the card
perms_group = 'perms_group_example' # str | Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK| 
card_print = 'card_print_example' # str | Card appareance code, also used to choose the program ID of the card
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
card_tag = 'card_tag_example' # str | Custom field (optional)
batch_delivery_id = 56 # int | Batch regroupement identifier (must be between 1 and 238327) NOT YET OPERATIONAL (optional)
limit_atm_year = 56 # int | ATM operations limit for a sliding year. No default value. (optional)
limit_atm_month = 56 # int | ATM operations limit for a sliding month. No default value. (optional)
limit_atm_week = 56 # int | ATM operations limit for a sliding week. Default value 2000â‚¬. (optional)
limit_atm_day = 56 # int | ATM operations limit for a single day. Default value 1000â‚¬. (optional)
limit_atm_all = 56 # int | ATM operations limit from beginning. No default value. (optional)
limit_payment_year = 56 # int | POS operations limit for a sliding year. No default value. (optional)
limit_payment_month = 56 # int | POS operations limit for a sliding month. No default value. (optional)
limit_payment_week = 56 # int | POS operations limit for a sliding week. Default value 3000â‚¬. (optional)
limit_payment_day = 56 # int | POS operations limit for a single day. Default value 2000â‚¬. (optional)
limit_payment_all = 56 # int | POS operations limit from beginning. No default value. (optional)
payment_daily_limit = 3.4 # float | POS operations limit for a single day including cents. The decimal delimiter must be \".\". No default value. (optional)
pin = 'pin_example' # str | Card's PIN code value. (optional)
anonymous = 56 # int | Card is anonymous. If value is 1 there will be no embossed name. (optional)
send_to_parent = 56 # int | If you put the value 1 the delivery address will be the parent user's. (optional)
mcc_restriction_group_id = 56 # int | mccRestrictionGroupId you want to apply (optional)
merchant_restriction_group_id = 56 # int | merchantRestrictionGroupId you want to apply (optional)
country_restriction_group_id = 56 # int | countryRestrictionGroupId you want to apply (optional)
emboss_legal_name = false # bool | Set true if you want emboss the legal name. (optional) (default to false)
logo_id = 'logo_id_example' # str | logoId for co-branding. Can't be more than 30 characters (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
package_id = NULL # object | Packaging reference for card shipping. Maximum 8 characters (optional)

try:
    # create a virtual card
    api_response = api_instance.cards_create_virtual_post(user_id, wallet_id, perms_group, card_print, access_signature=access_signature, card_tag=card_tag, batch_delivery_id=batch_delivery_id, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, pin=pin, anonymous=anonymous, send_to_parent=send_to_parent, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, emboss_legal_name=emboss_legal_name, logo_id=logo_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, package_id=package_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_create_virtual_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User&#39;s id of cardholder | 
 **wallet_id** | **int**| Wallet&#39;s Id in which to create the card | 
 **perms_group** | **str**| Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK|  | 
 **card_print** | **str**| Card appareance code, also used to choose the program ID of the card | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **card_tag** | **str**| Custom field | [optional] 
 **batch_delivery_id** | **int**| Batch regroupement identifier (must be between 1 and 238327) NOT YET OPERATIONAL | [optional] 
 **limit_atm_year** | **int**| ATM operations limit for a sliding year. No default value. | [optional] 
 **limit_atm_month** | **int**| ATM operations limit for a sliding month. No default value. | [optional] 
 **limit_atm_week** | **int**| ATM operations limit for a sliding week. Default value 2000â‚¬. | [optional] 
 **limit_atm_day** | **int**| ATM operations limit for a single day. Default value 1000â‚¬. | [optional] 
 **limit_atm_all** | **int**| ATM operations limit from beginning. No default value. | [optional] 
 **limit_payment_year** | **int**| POS operations limit for a sliding year. No default value. | [optional] 
 **limit_payment_month** | **int**| POS operations limit for a sliding month. No default value. | [optional] 
 **limit_payment_week** | **int**| POS operations limit for a sliding week. Default value 3000â‚¬. | [optional] 
 **limit_payment_day** | **int**| POS operations limit for a single day. Default value 2000â‚¬. | [optional] 
 **limit_payment_all** | **int**| POS operations limit from beginning. No default value. | [optional] 
 **payment_daily_limit** | **float**| POS operations limit for a single day including cents. The decimal delimiter must be \&quot;.\&quot;. No default value. | [optional] 
 **pin** | **str**| Card&#39;s PIN code value. | [optional] 
 **anonymous** | **int**| Card is anonymous. If value is 1 there will be no embossed name. | [optional] 
 **send_to_parent** | **int**| If you put the value 1 the delivery address will be the parent user&#39;s. | [optional] 
 **mcc_restriction_group_id** | **int**| mccRestrictionGroupId you want to apply | [optional] 
 **merchant_restriction_group_id** | **int**| merchantRestrictionGroupId you want to apply | [optional] 
 **country_restriction_group_id** | **int**| countryRestrictionGroupId you want to apply | [optional] 
 **emboss_legal_name** | **bool**| Set true if you want emboss the legal name. | [optional] [default to false]
 **logo_id** | **str**| logoId for co-branding. Can&#39;t be more than 30 characters | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **package_id** | [**object**](.md)| Packaging reference for card shipping. Maximum 8 characters | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_get**
> object cards_get(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, card_id=card_id, user_id=user_id, embossed_name=embossed_name, public_token=public_token, perms_group=perms_group, is_physical=is_physical, is_converted=is_converted, lock_status=lock_status, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search cards

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
card_id = 56 # int | Card's id (optional)
user_id = 56 # int | User's id of cardholder (optional)
embossed_name = 'embossed_name_example' # str | Embossed name of the card (optional)
public_token = 'public_token_example' # str | Public token of the card (optional)
perms_group = 'perms_group_example' # str | Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK|  (optional)
is_physical = 56 # int | | Value | Type | | --- | --- | | 1 | Physical card| | 0 | Virtual card |  (optional)
is_converted = 56 # int | | Value | Type | | --- | --- | | 1 | Physical card converted in a virtual card| | 0 | Not converted |  (optional)
lock_status = 56 # int | | Value | Type | | --- | --- | | 1 | Card blocked | | 0 | Card Unblocked | | 2 | Lost card | | 3 | Stolen card |  (optional)
mcc_restriction_group_id = 56 # int | mccRestrictionGroupId of the card (optional)
merchant_restriction_group_id = 56 # int | merchantRestrictionGroupId of the card (optional)
country_restriction_group_id = 56 # int | countryRestrictionGroupId of the card (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search cards
    api_response = api_instance.cards_get(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, card_id=card_id, user_id=user_id, embossed_name=embossed_name, public_token=public_token, perms_group=perms_group, is_physical=is_physical, is_converted=is_converted, lock_status=lock_status, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **card_id** | **int**| Card&#39;s id | [optional] 
 **user_id** | **int**| User&#39;s id of cardholder | [optional] 
 **embossed_name** | **str**| Embossed name of the card | [optional] 
 **public_token** | **str**| Public token of the card | [optional] 
 **perms_group** | **str**| Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK|  | [optional] 
 **is_physical** | **int**| | Value | Type | | --- | --- | | 1 | Physical card| | 0 | Virtual card |  | [optional] 
 **is_converted** | **int**| | Value | Type | | --- | --- | | 1 | Physical card converted in a virtual card| | 0 | Not converted |  | [optional] 
 **lock_status** | **int**| | Value | Type | | --- | --- | | 1 | Card blocked | | 0 | Card Unblocked | | 2 | Lost card | | 3 | Stolen card |  | [optional] 
 **mcc_restriction_group_id** | **int**| mccRestrictionGroupId of the card | [optional] 
 **merchant_restriction_group_id** | **int**| merchantRestrictionGroupId of the card | [optional] 
 **country_restriction_group_id** | **int**| countryRestrictionGroupId of the card | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_activate_put**
> object cards_id_activate_put(id, access_token=access_token, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

activate a card

The Activate endpoint change card status to activate. An activated card can be freely used. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_token = 'access_token_example' # str | Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # activate a card
    api_response = api_instance.cards_id_activate_put(id, access_token=access_token, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_activate_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_token** | **str**| Access token must be defined here or in Authorization HTTP header. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_change_pin_put**
> object cards_id_change_pin_put(id, current_pin, new_pin, confirm_pin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

change card's PIN

Change card's PIN code knowing the current one.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
current_pin = 'current_pin_example' # str | Card's current PIN
new_pin = 'new_pin_example' # str | Card's new PIN
confirm_pin = 'confirm_pin_example' # str | Card's new PIN confirmation value
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # change card's PIN
    api_response = api_instance.cards_id_change_pin_put(id, current_pin, new_pin, confirm_pin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_change_pin_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **current_pin** | **str**| Card&#39;s current PIN | 
 **new_pin** | **str**| Card&#39;s new PIN | 
 **confirm_pin** | **str**| Card&#39;s new PIN confirmation value | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_convert_virtual_put**
> object cards_id_convert_virtual_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

convert card to virtual

Convert a virtual card into a physical one. The converted card will be both virtual and physical. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # convert card to virtual
    api_response = api_instance.cards_id_convert_virtual_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_convert_virtual_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_get**
> object cards_id_get(id)

get a card

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 789 # int | Card's id.

try:
    # get a card
    api_response = api_instance.cards_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_limits_put**
> object cards_id_limits_put(id, access_signature=access_signature, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, restriction_group_limits=restriction_group_limits, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

update card's limits

Update of the card limits.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
limit_atm_year = 56 # int | ATM year limit (optional)
limit_atm_month = 56 # int | ATM month limit (optional)
limit_atm_week = 56 # int | ATM week limit (optional)
limit_atm_day = 56 # int | ATM day limit (optional)
limit_atm_all = 56 # int | ATM from beginning limit (optional)
limit_payment_year = 56 # int | Payment year limit (optional)
limit_payment_month = 56 # int | Payment month limit (optional)
limit_payment_week = 56 # int | Payment week limit (optional)
limit_payment_day = 56 # int | Payment day limit (optional)
limit_payment_all = 56 # int | Payment from beginning limit (optional)
payment_daily_limit = 3.4 # float | Payment day limit including cents. The decimal delimiter must be \".\" (optional)
restriction_group_limits = NULL # list[object] | Group of limits based on Restriction Groups. By now, only Restaurant Payment Vouchers with paymentDailyLimit can use this field. Furthermore, one of `mccRestrictionGroups`, `countryRestrictionGroups` or `merchantIdRestrictionGroups` must be present (i.e. `paymentDailyLimit` can't be alone). (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # update card's limits
    api_response = api_instance.cards_id_limits_put(id, access_signature=access_signature, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, restriction_group_limits=restriction_group_limits, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_limits_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **limit_atm_year** | **int**| ATM year limit | [optional] 
 **limit_atm_month** | **int**| ATM month limit | [optional] 
 **limit_atm_week** | **int**| ATM week limit | [optional] 
 **limit_atm_day** | **int**| ATM day limit | [optional] 
 **limit_atm_all** | **int**| ATM from beginning limit | [optional] 
 **limit_payment_year** | **int**| Payment year limit | [optional] 
 **limit_payment_month** | **int**| Payment month limit | [optional] 
 **limit_payment_week** | **int**| Payment week limit | [optional] 
 **limit_payment_day** | **int**| Payment day limit | [optional] 
 **limit_payment_all** | **int**| Payment from beginning limit | [optional] 
 **payment_daily_limit** | **float**| Payment day limit including cents. The decimal delimiter must be \&quot;.\&quot; | [optional] 
 **restriction_group_limits** | [**list[object]**](object.md)| Group of limits based on Restriction Groups. By now, only Restaurant Payment Vouchers with paymentDailyLimit can use this field. Furthermore, one of &#x60;mccRestrictionGroups&#x60;, &#x60;countryRestrictionGroups&#x60; or &#x60;merchantIdRestrictionGroups&#x60; must be present (i.e. &#x60;paymentDailyLimit&#x60; can&#39;t be alone). | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_lock_unlock_put**
> object cards_id_lock_unlock_put(id, lock_status, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

update card's blocking status

Block or unblock a card.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
lock_status = 56 # int | | Value | Type | | --- | --- | | 1 | Block the card| | 0 | Unblock the card | | 2 | Lost card | | 3 | Stolen card | 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # update card's blocking status
    api_response = api_instance.cards_id_lock_unlock_put(id, lock_status, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_lock_unlock_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **lock_status** | **int**| | Value | Type | | --- | --- | | 1 | Block the card| | 0 | Unblock the card | | 2 | Lost card | | 3 | Stolen card |  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_options_put**
> object cards_id_options_put(id, foreign, online, atm, nfc, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

update card's options

Allows to update card's permission group.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
foreign = 56 # int | Card usable abroad
online = 56 # int | Card usable on-line
atm = 56 # int | Card usable on ATM (withdrawals)
nfc = 56 # int | Card usable on contactless compatible POS machine machine\"
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # update card's options
    api_response = api_instance.cards_id_options_put(id, foreign, online, atm, nfc, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_options_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **foreign** | **int**| Card usable abroad | 
 **online** | **int**| Card usable on-line | 
 **atm** | **int**| Card usable on ATM (withdrawals) | 
 **nfc** | **int**| Card usable on contactless compatible POS machine machine\&quot; | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_put**
> object cards_id_put(id, access_signature=access_signature, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

update card informations

Actually update mccRestrictionGroupId and merchantRestrictionGroupId of a card

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
mcc_restriction_group_id = 56 # int | card's mccRestrictionGroupId (optional)
merchant_restriction_group_id = 56 # int | card's merchantRestrictionGroupId (optional)
country_restriction_group_id = 56 # int | card's countryRestrictionGroupId (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # update card informations
    api_response = api_instance.cards_id_put(id, access_signature=access_signature, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **mcc_restriction_group_id** | **int**| card&#39;s mccRestrictionGroupId | [optional] 
 **merchant_restriction_group_id** | **int**| card&#39;s merchantRestrictionGroupId | [optional] 
 **country_restriction_group_id** | **int**| card&#39;s countryRestrictionGroupId | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_regenerate_put**
> object cards_id_regenerate_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

regenerate card

only recreate the card image if the card is unlocked. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # regenerate card
    api_response = api_instance.cards_id_regenerate_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_regenerate_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_set_pin_put**
> object cards_id_set_pin_put(id, new_pin, confirm_pin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

set card's PIN

Overwrite card's PIN.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
new_pin = 'new_pin_example' # str | Card's new PIN.
confirm_pin = 'confirm_pin_example' # str | Card's new PIN confirmation value
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # set card's PIN
    api_response = api_instance.cards_id_set_pin_put(id, new_pin, confirm_pin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_set_pin_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **new_pin** | **str**| Card&#39;s new PIN. | 
 **confirm_pin** | **str**| Card&#39;s new PIN confirmation value | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_id_unblock_pin_put**
> object cards_id_unblock_pin_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

unblock card's PIN

Unblock card's PIN code.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
id = 56 # int | Card's id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # unblock card's PIN
    api_response = api_instance.cards_id_unblock_pin_put(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_id_unblock_pin_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card&#39;s id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_register3_ds_post**
> object cards_register3_ds_post(card_id, access_signature=access_signature)

Register 3D secure

Register a card to 3D secure service. The user's mobile number must begin by \"+\", or \"00\", and the country dialing code.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
card_id = 56 # int | Card's ID to register
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). (optional)

try:
    # Register 3D secure
    api_response = api_instance.cards_register3_ds_post(card_id, access_signature=access_signature)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_register3_ds_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **card_id** | **int**| Card&#39;s ID to register | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **cards_request_physical_post**
> object cards_request_physical_post(user_id, wallet_id, perms_group, card_tag, card_print, access_signature=access_signature, batch_delivery_id=batch_delivery_id, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, pin=pin, anonymous=anonymous, send_to_parent=send_to_parent, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, emboss_legal_name=emboss_legal_name, logo_id=logo_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, package_id=package_id)

create a physical card

Create a new physical card. To use a card you will need to activate it (/cards/{id}/Activate/).

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardApi(swagger_client.ApiClient(configuration))
user_id = 56 # int | User's id of cardholder
wallet_id = 56 # int | Wallet's Id in which to create the card
perms_group = 'perms_group_example' # str | Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK| 
card_tag = 'card_tag_example' # str | Custom field
card_print = 'card_print_example' # str | Card appearance code, also used to choose the program ID of the card
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
batch_delivery_id = 56 # int | Batch regroupement identifier (must be between 1 and 238327) NOT YET OPERATIONAL (optional)
limit_atm_year = 56 # int | ATM operations limit for a sliding year. No default value. (optional)
limit_atm_month = 56 # int | ATM operations limit for a sliding month. No default value. (optional)
limit_atm_week = 56 # int | ATM operations limit for a sliding week. Default value 2000â‚¬. (optional)
limit_atm_day = 56 # int | ATM operations limit for a single day. Default value 1000â‚¬. (optional)
limit_atm_all = 56 # int | ATM operations limit from beginning. No default value. (optional)
limit_payment_year = 56 # int | POS operations limit for a sliding year. No default value. (optional)
limit_payment_month = 56 # int | POS operations limit for a sliding month. No default value. (optional)
limit_payment_week = 56 # int | POS operations limit for a sliding week. Default value 3000â‚¬. (optional)
limit_payment_day = 56 # int | POS operations limit for a single day. Default value 2000â‚¬. (optional)
limit_payment_all = 56 # int | POS operations limit from beginning. No default value. (optional)
payment_daily_limit = 3.4 # float | POS operations limit for a single day including cents. The decimal delimiter must be \".\". No default value. (optional)
pin = 'pin_example' # str | Card's PIN code value. Default random PIN. (optional)
anonymous = 56 # int | Card is anonymous. If value is 1 there will be no embossed name. (optional)
send_to_parent = 56 # int | If you put the value 1 the delivery address will be the parent user's. (optional)
mcc_restriction_group_id = 56 # int | mccRestrictionGroupId you want to apply (optional)
merchant_restriction_group_id = 56 # int | merchantRestrictionGroupId you want to apply (optional)
country_restriction_group_id = 56 # int | countryRestrictionGroupId you want to apply (optional)
emboss_legal_name = false # bool | Set true if you want emboss the legal name. (optional) (default to false)
logo_id = 'logo_id_example' # str | logoId for co-branding. Can't be more than 30 characters (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
package_id = NULL # object | Packaging reference for card shipping. Maximum 8 characters (optional)

try:
    # create a physical card
    api_response = api_instance.cards_request_physical_post(user_id, wallet_id, perms_group, card_tag, card_print, access_signature=access_signature, batch_delivery_id=batch_delivery_id, limit_atm_year=limit_atm_year, limit_atm_month=limit_atm_month, limit_atm_week=limit_atm_week, limit_atm_day=limit_atm_day, limit_atm_all=limit_atm_all, limit_payment_year=limit_payment_year, limit_payment_month=limit_payment_month, limit_payment_week=limit_payment_week, limit_payment_day=limit_payment_day, limit_payment_all=limit_payment_all, payment_daily_limit=payment_daily_limit, pin=pin, anonymous=anonymous, send_to_parent=send_to_parent, mcc_restriction_group_id=mcc_restriction_group_id, merchant_restriction_group_id=merchant_restriction_group_id, country_restriction_group_id=country_restriction_group_id, emboss_legal_name=emboss_legal_name, logo_id=logo_id, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, package_id=package_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardApi->cards_request_physical_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| User&#39;s id of cardholder | 
 **wallet_id** | **int**| Wallet&#39;s Id in which to create the card | 
 **perms_group** | **str**| Permissions group of the card. There is 4 usages which create 16 possible groups:  | group | NFC | ATM | On-line | Foreign | |-----|-----|-----|-----|------| |TRZ-CU-001|KO|KO|KO|KO| |TRZ-CU-002|KO|KO|KO|OK| |TRZ-CU-003|KO|KO|OK|KO| |TRZ-CU-004|KO|KO|OK|OK| |TRZ-CU-005|KO|OK|KO|KO| |TRZ-CU-006|KO|OK|KO|OK| |TRZ-CU-007|KO|OK|OK|KO| |TRZ-CU-008|KO|OK|OK|OK| |TRZ-CU-009|OK|KO|KO|KO| |TRZ-CU-010|OK|KO|KO|OK| |TRZ-CU-011|OK|KO|OK|KO| |TRZ-CU-012|OK|KO|OK|OK| |TRZ-CU-013|OK|OK|KO|KO| |TRZ-CU-014|OK|OK|KO|OK| |TRZ-CU-015|OK|OK|OK|KO| |TRZ-CU-016|OK|OK|OK|OK|  | 
 **card_tag** | **str**| Custom field | 
 **card_print** | **str**| Card appearance code, also used to choose the program ID of the card | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **batch_delivery_id** | **int**| Batch regroupement identifier (must be between 1 and 238327) NOT YET OPERATIONAL | [optional] 
 **limit_atm_year** | **int**| ATM operations limit for a sliding year. No default value. | [optional] 
 **limit_atm_month** | **int**| ATM operations limit for a sliding month. No default value. | [optional] 
 **limit_atm_week** | **int**| ATM operations limit for a sliding week. Default value 2000â‚¬. | [optional] 
 **limit_atm_day** | **int**| ATM operations limit for a single day. Default value 1000â‚¬. | [optional] 
 **limit_atm_all** | **int**| ATM operations limit from beginning. No default value. | [optional] 
 **limit_payment_year** | **int**| POS operations limit for a sliding year. No default value. | [optional] 
 **limit_payment_month** | **int**| POS operations limit for a sliding month. No default value. | [optional] 
 **limit_payment_week** | **int**| POS operations limit for a sliding week. Default value 3000â‚¬. | [optional] 
 **limit_payment_day** | **int**| POS operations limit for a single day. Default value 2000â‚¬. | [optional] 
 **limit_payment_all** | **int**| POS operations limit from beginning. No default value. | [optional] 
 **payment_daily_limit** | **float**| POS operations limit for a single day including cents. The decimal delimiter must be \&quot;.\&quot;. No default value. | [optional] 
 **pin** | **str**| Card&#39;s PIN code value. Default random PIN. | [optional] 
 **anonymous** | **int**| Card is anonymous. If value is 1 there will be no embossed name. | [optional] 
 **send_to_parent** | **int**| If you put the value 1 the delivery address will be the parent user&#39;s. | [optional] 
 **mcc_restriction_group_id** | **int**| mccRestrictionGroupId you want to apply | [optional] 
 **merchant_restriction_group_id** | **int**| merchantRestrictionGroupId you want to apply | [optional] 
 **country_restriction_group_id** | **int**| countryRestrictionGroupId you want to apply | [optional] 
 **emboss_legal_name** | **bool**| Set true if you want emboss the legal name. | [optional] [default to false]
 **logo_id** | **str**| logoId for co-branding. Can&#39;t be more than 30 characters | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **package_id** | [**object**](.md)| Packaging reference for card shipping. Maximum 8 characters | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


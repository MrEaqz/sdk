# swagger_client.CardtransactionApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_cardtransaction**](CardtransactionApi.md#get_cardtransaction) | **GET** /cardtransactions/{id} | get a card transaction
[**read_card_transaction**](CardtransactionApi.md#read_card_transaction) | **GET** /cardtransactions | search for card transactions


# **get_cardtransaction**
> object get_cardtransaction(id)

get a card transaction

get a card transaction

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardtransactionApi(swagger_client.ApiClient(configuration))
id = 789 # int | Card transactions's internal id.

try:
    # get a card transaction
    api_response = api_instance.get_cardtransaction(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardtransactionApi->get_cardtransaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Card transactions&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **read_card_transaction**
> object read_card_transaction(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, cardtransaction_id=cardtransaction_id, card_id=card_id, wallet_id=wallet_id, merchant_id=merchant_id, public_token=public_token, payment_id=payment_id, page_number=page_number, page_count=page_count, sort_by=sort_by)

search for card transactions

Search for documents. The request must contain at least one of those inputs - cardId, paymentId, publicToken, walletId

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.CardtransactionApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
cardtransaction_id = 56 # int | Card transaction's Id (optional)
card_id = 56 # int | Card's Id (optional)
wallet_id = 56 # int | Card's wallet Id (optional)
merchant_id = 56 # int | Merchant's Id (optional)
public_token = 'public_token_example' # str | Card's public token (optional)
payment_id = 56 # int | Payment's Id (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists). required: false (optional)

try:
    # search for card transactions
    api_response = api_instance.read_card_transaction(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, cardtransaction_id=cardtransaction_id, card_id=card_id, wallet_id=wallet_id, merchant_id=merchant_id, public_token=public_token, payment_id=payment_id, page_number=page_number, page_count=page_count, sort_by=sort_by)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CardtransactionApi->read_card_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **cardtransaction_id** | **int**| Card transaction&#39;s Id | [optional] 
 **card_id** | **int**| Card&#39;s Id | [optional] 
 **wallet_id** | **int**| Card&#39;s wallet Id | [optional] 
 **merchant_id** | **int**| Merchant&#39;s Id | [optional] 
 **public_token** | **str**| Card&#39;s public token | [optional] 
 **payment_id** | **int**| Payment&#39;s Id | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists). required: false | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


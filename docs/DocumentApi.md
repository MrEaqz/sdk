# swagger_client.DocumentApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_documents**](DocumentApi.md#create_documents) | **POST** /documents | create a document
[**delete_document**](DocumentApi.md#delete_document) | **DELETE** /documents/{id} | delete document
[**get_document**](DocumentApi.md#get_document) | **GET** /documents/{id} | get a document
[**get_documents**](DocumentApi.md#get_documents) | **GET** /documents | search documents
[**put_document**](DocumentApi.md#put_document) | **PUT** /documents/{id} | update a document


# **create_documents**
> object create_documents(user_id, document_type_id, name, file_content_base64, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, document_tag=document_tag, residence_id=residence_id)

create a document

Search for documents.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DocumentApi(swagger_client.ApiClient(configuration))
user_id = 'user_id_example' # str | Document user's id.
document_type_id = 56 # int | Type of document.  | documentTypeId | Description | | --- | --- | | 2 | Police record | | 4 | Company Registration | | 6 | CV | | 7 | Sworn statement | | 8 | Turnover | | 9 | Identity card | | 11 | Bank Identity Statement | | 12 | Proof of address| | 13 | Mobile phone invoice| | 14 | Invoice, other than Mobile phone invoice| | 15 | A residence permit| | 16 | A driving licence| | 17 | A passport| | 18 | A proxy granting an employee| | 19 | A company registration official paper| | 20 | Official tax certificate| | 21 | Employee payment notice| | 22 | User bank statement| | 23 | Business legal status| | 24 | Tax Statement| | 25 | Exemption Statement| | 26 | Liveness result| | 27 | Health insurance card| 
name = 'name_example' # str | Document's name.
file_content_base64 = 'B' # str | Document file content. base64 encoded.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
document_tag = 56 # int | Custom data. (optional)
residence_id = 789 # int | Document residence Id. The field is mandatory when the document is of type id 24 or 25. Otherwise it should not be provided. (optional)

try:
    # create a document
    api_response = api_instance.create_documents(user_id, document_type_id, name, file_content_base64, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, document_tag=document_tag, residence_id=residence_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocumentApi->create_documents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| Document user&#39;s id. | 
 **document_type_id** | **int**| Type of document.  | documentTypeId | Description | | --- | --- | | 2 | Police record | | 4 | Company Registration | | 6 | CV | | 7 | Sworn statement | | 8 | Turnover | | 9 | Identity card | | 11 | Bank Identity Statement | | 12 | Proof of address| | 13 | Mobile phone invoice| | 14 | Invoice, other than Mobile phone invoice| | 15 | A residence permit| | 16 | A driving licence| | 17 | A passport| | 18 | A proxy granting an employee| | 19 | A company registration official paper| | 20 | Official tax certificate| | 21 | Employee payment notice| | 22 | User bank statement| | 23 | Business legal status| | 24 | Tax Statement| | 25 | Exemption Statement| | 26 | Liveness result| | 27 | Health insurance card|  | 
 **name** | **str**| Document&#39;s name. | 
 **file_content_base64** | **str**| Document file content. base64 encoded. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **document_tag** | **int**| Custom data. | [optional] 
 **residence_id** | **int**| Document residence Id. The field is mandatory when the document is of type id 24 or 25. Otherwise it should not be provided. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_document**
> object delete_document(id)

delete document

Remove a document from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DocumentApi(swagger_client.ApiClient(configuration))
id = 789 # int | Document's internal id.

try:
    # delete document
    api_response = api_instance.delete_document(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocumentApi->delete_document: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Document&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_document**
> object get_document(id)

get a document

get a document

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DocumentApi(swagger_client.ApiClient(configuration))
id = 789 # int | Document's internal id.

try:
    # get a document
    api_response = api_instance.get_document(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocumentApi->get_document: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Document&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_documents**
> object get_documents(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, document_id=document_id, document_tag=document_tag, document_status=document_status, document_type_id=document_type_id, document_type=document_type, user_id=user_id, user_name=user_name, user_email=user_email, file_name=file_name, file_size=file_size, file_type=file_type, is_agent=is_agent, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search documents

Search for documents.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DocumentApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
document_id = 56 # int | Document's unique id. (optional)
document_tag = 'document_tag_example' # str | Custom data. (optional)
document_status = 'document_status_example' # str | The status of the document. Possible values: * PENDING * CANCELED * VALIDATED  (optional)
document_type_id = 56 # int | Document type id. (optional)
document_type = 'document_type_example' # str | Document's type. (optional)
user_id = 56 # int | Document user's id. (optional)
user_name = 'user_name_example' # str | Document user's name. (optional)
user_email = 'user_email_example' # str | Document user's email. (optional)
file_name = 'file_name_example' # str | Document's name. (optional)
file_size = 56 # int | Document's size. (optional)
file_type = 56 # int | Document's type. (optional)
is_agent = 'is_agent_example' # str | is agent field. (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search documents
    api_response = api_instance.get_documents(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, document_id=document_id, document_tag=document_tag, document_status=document_status, document_type_id=document_type_id, document_type=document_type, user_id=user_id, user_name=user_name, user_email=user_email, file_name=file_name, file_size=file_size, file_type=file_type, is_agent=is_agent, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocumentApi->get_documents: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **document_id** | **int**| Document&#39;s unique id. | [optional] 
 **document_tag** | **str**| Custom data. | [optional] 
 **document_status** | **str**| The status of the document. Possible values: * PENDING * CANCELED * VALIDATED  | [optional] 
 **document_type_id** | **int**| Document type id. | [optional] 
 **document_type** | **str**| Document&#39;s type. | [optional] 
 **user_id** | **int**| Document user&#39;s id. | [optional] 
 **user_name** | **str**| Document user&#39;s name. | [optional] 
 **user_email** | **str**| Document user&#39;s email. | [optional] 
 **file_name** | **str**| Document&#39;s name. | [optional] 
 **file_size** | **int**| Document&#39;s size. | [optional] 
 **file_type** | **int**| Document&#39;s type. | [optional] 
 **is_agent** | **str**| is agent field. | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_document**
> object put_document(id)

update a document

Update a document.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DocumentApi(swagger_client.ApiClient(configuration))
id = 789 # int | Document's internal id.

try:
    # update a document
    api_response = api_instance.put_document(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocumentApi->put_document: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Document&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# swagger_client.PayinApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_payin**](PayinApi.md#delete_payin) | **DELETE** /payins/{id} | delete a pay in
[**get_payin**](PayinApi.md#get_payin) | **GET** /payins/{id} | get a pay in
[**get_payins**](PayinApi.md#get_payins) | **GET** /payins | search pay ins
[**post_payin**](PayinApi.md#post_payin) | **POST** /payins | create a pay in


# **delete_payin**
> object delete_payin(id)

delete a pay in

Deactivate a payin in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinApi(swagger_client.ApiClient(configuration))
id = 789 # int | Payin's id.

try:
    # delete a pay in
    api_response = api_instance.delete_payin(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinApi->delete_payin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Payin&#39;s id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_payin**
> object get_payin(id)

get a pay in

Get a payin from the system by its id.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinApi(swagger_client.ApiClient(configuration))
id = 789 # int | Payin's id.

try:
    # get a pay in
    api_response = api_instance.get_payin(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinApi->get_payin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Payin&#39;s id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_payins**
> object get_payins(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_id=payin_id, wallet_id=wallet_id, payin_tag=payin_tag, payin_status=payin_status, user_id=user_id, user_name=user_name, user_email=user_email, beneficiary_user_id=beneficiary_user_id, event_alias=event_alias, wallet_type_id=wallet_type_id, payment_method_id=payment_method_id, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order)

search pay ins

Get payins that match search criteria.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 56 # int | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
payin_id = 56 # int | Payin's id. (optional)
wallet_id = 56 # int | Payin's wallet id. (optional)
payin_tag = 'payin_tag_example' # str | Client custom data. (optional)
payin_status = 'payin_status_example' # str | Payins's status. (optional)
user_id = 56 # int | User's id who performed the operation (debited). (optional)
user_name = 'user_name_example' # str | User's name who performed the operation (debited). (optional)
user_email = 'user_email_example' # str | User's email who performed the operation (debited). (optional)
beneficiary_user_id = 56 # int | User's id who received the operation (credited). (optional)
event_alias = 'event_alias_example' # str | Wallet eventAlias of the payin (credited wallet). (optional)
wallet_type_id = 56 # int | Payin's wallet type id. (optional)
payment_method_id = 'payment_method_id_example' # str | Payin's payment method id. (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : createdDate. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)

try:
    # search pay ins
    api_response = api_instance.get_payins(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_id=payin_id, wallet_id=wallet_id, payin_tag=payin_tag, payin_status=payin_status, user_id=user_id, user_name=user_name, user_email=user_email, beneficiary_user_id=beneficiary_user_id, event_alias=event_alias, wallet_type_id=wallet_type_id, payment_method_id=payment_method_id, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinApi->get_payins: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **int**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **payin_id** | **int**| Payin&#39;s id. | [optional] 
 **wallet_id** | **int**| Payin&#39;s wallet id. | [optional] 
 **payin_tag** | **str**| Client custom data. | [optional] 
 **payin_status** | **str**| Payins&#39;s status. | [optional] 
 **user_id** | **int**| User&#39;s id who performed the operation (debited). | [optional] 
 **user_name** | **str**| User&#39;s name who performed the operation (debited). | [optional] 
 **user_email** | **str**| User&#39;s email who performed the operation (debited). | [optional] 
 **beneficiary_user_id** | **int**| User&#39;s id who received the operation (credited). | [optional] 
 **event_alias** | **str**| Wallet eventAlias of the payin (credited wallet). | [optional] 
 **wallet_type_id** | **int**| Payin&#39;s wallet type id. | [optional] 
 **payment_method_id** | **str**| Payin&#39;s payment method id. | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : createdDate. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_payin**
> object post_payin(wallet_id, user_id, payment_method_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_tag=payin_tag, oneclickcard_id=oneclickcard_id, payment_accepted_url=payment_accepted_url, payment_waiting_url=payment_waiting_url, payment_refused_url=payment_refused_url, payment_canceled_url=payment_canceled_url, payment_exception_url=payment_exception_url, distributor_fee=distributor_fee, message_to_user=message_to_user, language=language, created_ip=created_ip, payin_date=payin_date, mandate_id=mandate_id)

create a pay in

Create a new pay in in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.PayinApi(swagger_client.ApiClient(configuration))
wallet_id = 56 # int | Credited wallet's ID
user_id = 56 # int | User's id who makes the pay in. NB : this parameter should should not be transmitted in the case of payin of method Sepa Direct Debit Core (21)   OR Cheque (26). It will be set automatically by the system. 
payment_method_id = 56 # int | | Id | Payment by | | ---| --- | | 11 | Card | | 14 | Oneclick card (without payment form) | | 21 | Sepa Direct Debit Core | | 23 | Full Hosted HTML Payment Form. Please note that if you use this paymentMethodId the flieds paymentAcceptedUrl, paymentRefusedUrl, paymentWaitingUrl, paymentCanceledUrl and paymentExceptionUrl are mandatory. CSS of the payment page can be customized | | 24 | IFrame Payment Form. Please note that if you use this paymentMethodId the flieds paymentAcceptedUrl, paymentRefusedUrl, paymentWaitingUrl, paymentCanceledUrl and paymentExceptionUrl are mandatory. CSS of the payment page can be customized| | 25 | Payment made through an SDK - You cannot directly create a payin directly with this method id. The payin will be automatically created by the system. | | 26 | Cheque | 
amount = 3.4 # float | Pay in amount.
currency = 'currency_example' # str | Payin currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). Must be the same as the wallet's. 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
payin_tag = 'payin_tag_example' # str | Client custom data. (optional)
oneclickcard_id = 56 # int | Oneclick card's id. Mandatory if payment method is 14. Useless otherwise. (optional)
payment_accepted_url = 'payment_accepted_url_example' # str | Url where cardholder is redirected if payment is successful. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. (optional)
payment_waiting_url = 'payment_waiting_url_example' # str | Url where cardholder is redirected to wait payment processing. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. (optional)
payment_refused_url = 'payment_refused_url_example' # str | Url where cardholder is redirected if payment is refused. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. (optional)
payment_canceled_url = 'payment_canceled_url_example' # str | Url where cardholder is redirected if payment is canceled. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. (optional)
payment_exception_url = 'payment_exception_url_example' # str | Url where cardholder is redirected if the payment process raised an exception. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. (optional)
distributor_fee = 3.4 # float | Distributor fee (optional)
message_to_user = 'message_to_user_example' # str | Message to send to wallet's user. In the case of a Sepa Direct Debit Core payment method this parameter will be the reconciliation information transmitted to the Debtor (ie. Invoice number ...). In this case it cannot be more than 140 characters. (optional)
language = 'language_example' # str | Language for the third party interface. (optional)
created_ip = 'created_ip_example' # str | User's IP address (optional)
payin_date = 'payin_date_example' # str | The date at which the SDD should be presented. This parameter is mandatory when performing a payin of method SDD Core. Format is YYYY-MM-DD The date should follow some requirements (Traget 2 working day) :    - Be a weekday (Monday to Friday)   - January 1st is excluded   - May 1st is excluded   - December 25 is excluded   - December 26th is excluded   - Easter day is excluded   - Easter Monday is excluded   - Good Friday is excluded  (optional)
mandate_id = 56 # int | The id of the mandate. This parameter is mandatory when performing a payin with method SDD Core. (optional)

try:
    # create a pay in
    api_response = api_instance.post_payin(wallet_id, user_id, payment_method_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, payin_tag=payin_tag, oneclickcard_id=oneclickcard_id, payment_accepted_url=payment_accepted_url, payment_waiting_url=payment_waiting_url, payment_refused_url=payment_refused_url, payment_canceled_url=payment_canceled_url, payment_exception_url=payment_exception_url, distributor_fee=distributor_fee, message_to_user=message_to_user, language=language, created_ip=created_ip, payin_date=payin_date, mandate_id=mandate_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PayinApi->post_payin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **int**| Credited wallet&#39;s ID | 
 **user_id** | **int**| User&#39;s id who makes the pay in. NB : this parameter should should not be transmitted in the case of payin of method Sepa Direct Debit Core (21)   OR Cheque (26). It will be set automatically by the system.  | 
 **payment_method_id** | **int**| | Id | Payment by | | ---| --- | | 11 | Card | | 14 | Oneclick card (without payment form) | | 21 | Sepa Direct Debit Core | | 23 | Full Hosted HTML Payment Form. Please note that if you use this paymentMethodId the flieds paymentAcceptedUrl, paymentRefusedUrl, paymentWaitingUrl, paymentCanceledUrl and paymentExceptionUrl are mandatory. CSS of the payment page can be customized | | 24 | IFrame Payment Form. Please note that if you use this paymentMethodId the flieds paymentAcceptedUrl, paymentRefusedUrl, paymentWaitingUrl, paymentCanceledUrl and paymentExceptionUrl are mandatory. CSS of the payment page can be customized| | 25 | Payment made through an SDK - You cannot directly create a payin directly with this method id. The payin will be automatically created by the system. | | 26 | Cheque |  | 
 **amount** | **float**| Pay in amount. | 
 **currency** | **str**| Payin currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). Must be the same as the wallet&#39;s.  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **payin_tag** | **str**| Client custom data. | [optional] 
 **oneclickcard_id** | **int**| Oneclick card&#39;s id. Mandatory if payment method is 14. Useless otherwise. | [optional] 
 **payment_accepted_url** | **str**| Url where cardholder is redirected if payment is successful. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. | [optional] 
 **payment_waiting_url** | **str**| Url where cardholder is redirected to wait payment processing. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. | [optional] 
 **payment_refused_url** | **str**| Url where cardholder is redirected if payment is refused. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. | [optional] 
 **payment_canceled_url** | **str**| Url where cardholder is redirected if payment is canceled. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. | [optional] 
 **payment_exception_url** | **str**| Url where cardholder is redirected if the payment process raised an exception. When using paymentMethodId 23 and 24 an HTTPS URL is mandatory. | [optional] 
 **distributor_fee** | **float**| Distributor fee | [optional] 
 **message_to_user** | **str**| Message to send to wallet&#39;s user. In the case of a Sepa Direct Debit Core payment method this parameter will be the reconciliation information transmitted to the Debtor (ie. Invoice number ...). In this case it cannot be more than 140 characters. | [optional] 
 **language** | **str**| Language for the third party interface. | [optional] 
 **created_ip** | **str**| User&#39;s IP address | [optional] 
 **payin_date** | **str**| The date at which the SDD should be presented. This parameter is mandatory when performing a payin of method SDD Core. Format is YYYY-MM-DD The date should follow some requirements (Traget 2 working day) :    - Be a weekday (Monday to Friday)   - January 1st is excluded   - May 1st is excluded   - December 25 is excluded   - December 26th is excluded   - Easter day is excluded   - Easter Monday is excluded   - Good Friday is excluded  | [optional] 
 **mandate_id** | **int**| The id of the mandate. This parameter is mandatory when performing a payin with method SDD Core. | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


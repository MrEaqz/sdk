# swagger_client.TransactionApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_transaction**](TransactionApi.md#get_transaction) | **GET** /transactions/{id} | get a transaction
[**get_transactions**](TransactionApi.md#get_transactions) | **GET** /transactions | search transactions


# **get_transaction**
> object get_transaction(id)

get a transaction

Get a transaction from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransactionApi(swagger_client.ApiClient(configuration))
id = 789 # int | Transaction's internal id.

try:
    # get a transaction
    api_response = api_instance.get_transaction(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransactionApi->get_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Transaction&#39;s internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transactions**
> object get_transactions(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transaction_id=transaction_id, transaction_type=transaction_type, wallet_id=wallet_id, user_id=user_id, name=name, description=description, amount=amount, currency=currency, value_date=value_date, execution_date=execution_date, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to)

search transactions

Get transactions that match search criteria. The request must contains at least one of those inputs walletId, transactionId,  executionDate, valueDate, createdDateFrom , createdDateTo

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransactionApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
transaction_id = 56 # int | Transaction's Id (optional)
transaction_type = 'transaction_type_example' # str | Transaction types. Possible values: * Payin * Payout * Transfer * Transfer Refund * Payin Refund * Discount * Bill  (optional)
wallet_id = 56 # int | Transactions' wallet id. (optional)
user_id = 56 # int | Transactions' user id. (optional)
name = 'name_example' # str | Transactions' name. (optional)
description = 'description_example' # str | transactions' description. (optional)
amount = 'amount_example' # str | Transactions' amount. (optional)
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (optional)
value_date = '2013-10-20T19:20:30+01:00' # datetime | The value date of the transaction (date applied for the payment) Format : YYYY-MM-DD HH:MM:SS (optional)
execution_date = '2013-10-20T19:20:30+01:00' # datetime | Date of the execution of the transaction Format : YYYY-MM-DD HH:MM:SS (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search transactions
    api_response = api_instance.get_transactions(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transaction_id=transaction_id, transaction_type=transaction_type, wallet_id=wallet_id, user_id=user_id, name=name, description=description, amount=amount, currency=currency, value_date=value_date, execution_date=execution_date, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransactionApi->get_transactions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **transaction_id** | **int**| Transaction&#39;s Id | [optional] 
 **transaction_type** | **str**| Transaction types. Possible values: * Payin * Payout * Transfer * Transfer Refund * Payin Refund * Discount * Bill  | [optional] 
 **wallet_id** | **int**| Transactions&#39; wallet id. | [optional] 
 **user_id** | **int**| Transactions&#39; user id. | [optional] 
 **name** | **str**| Transactions&#39; name. | [optional] 
 **description** | **str**| transactions&#39; description. | [optional] 
 **amount** | **str**| Transactions&#39; amount. | [optional] 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | [optional] 
 **value_date** | **datetime**| The value date of the transaction (date applied for the payment) Format : YYYY-MM-DD HH:MM:SS | [optional] 
 **execution_date** | **datetime**| Date of the execution of the transaction Format : YYYY-MM-DD HH:MM:SS | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


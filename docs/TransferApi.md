# swagger_client.TransferApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_transfer**](TransferApi.md#delete_transfer) | **DELETE** /transfers/{id} | cancel a transfer
[**get_transfer**](TransferApi.md#get_transfer) | **GET** /transfers/{id} | get a transfer
[**get_transfers**](TransferApi.md#get_transfers) | **GET** /transfers | search transfers
[**post_transfers**](TransferApi.md#post_transfers) | **POST** /transfers | create a transfer


# **delete_transfer**
> object delete_transfer(id)

cancel a transfer

Change transfer's status to CANCELED. A validated transfer can't be cancelled.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferApi(swagger_client.ApiClient(configuration))
id = 789 # int | Transfert internal id.

try:
    # cancel a transfer
    api_response = api_instance.delete_transfer(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferApi->delete_transfer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Transfert internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transfer**
> object get_transfer(id)

get a transfer

Get a transfert from the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferApi(swagger_client.ApiClient(configuration))
id = 789 # int | Transfers internal id.

try:
    # get a transfer
    api_response = api_instance.get_transfer(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferApi->get_transfer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Transfers internal id. | 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transfers**
> object get_transfers(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transfer_id=transfer_id, transfer_tag=transfer_tag, transfer_status=transfer_status, wallet_id=wallet_id, beneficiary_wallet_id=beneficiary_wallet_id, user_id=user_id, beneficiary_user_id=beneficiary_user_id, transfer_date=transfer_date, amount=amount, currency=currency, transfer_type_id=transfer_type_id, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search transfers

Search for transfers in the system. The request must contains at least one of those inputs  walletId, beneficiaryWalletId, userId, beneficiaryUserId, transferId, transferTag

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
transfer_id = 56 # int | Transfer's id. (optional)
transfer_tag = 'transfer_tag_example' # str | Custom data. (optional)
transfer_status = 'transfer_status_example' # str | Transfer's status. Possible values: * PENDING * CANCELED * VALIDATED  (optional)
wallet_id = 56 # int | Debited wallet's id. (optional)
beneficiary_wallet_id = 56 # int | Credited wallet's id. (optional)
user_id = 56 # int | Debited wallet user's id. (optional)
beneficiary_user_id = 56 # int | Credited wallet user's id. (optional)
transfer_date = '2013-10-20T19:20:30+01:00' # datetime | Transfert's date. Format : YYYY-MM-DD HH:MM:SS  (optional)
amount = 'amount_example' # str | Transfert's amount. (optional)
currency = 'currency_example' # str | Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (optional)
transfer_type_id = 56 # int | The Type Id of the Transfer:  | ID | Description | |-----|-----| | 1 | Wallet to wallet | | 2 | Card transaction | | 3 | Client fees | | 4 | Credit note |  (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search transfers
    api_response = api_instance.get_transfers(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transfer_id=transfer_id, transfer_tag=transfer_tag, transfer_status=transfer_status, wallet_id=wallet_id, beneficiary_wallet_id=beneficiary_wallet_id, user_id=user_id, beneficiary_user_id=beneficiary_user_id, transfer_date=transfer_date, amount=amount, currency=currency, transfer_type_id=transfer_type_id, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferApi->get_transfers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **transfer_id** | **int**| Transfer&#39;s id. | [optional] 
 **transfer_tag** | **str**| Custom data. | [optional] 
 **transfer_status** | **str**| Transfer&#39;s status. Possible values: * PENDING * CANCELED * VALIDATED  | [optional] 
 **wallet_id** | **int**| Debited wallet&#39;s id. | [optional] 
 **beneficiary_wallet_id** | **int**| Credited wallet&#39;s id. | [optional] 
 **user_id** | **int**| Debited wallet user&#39;s id. | [optional] 
 **beneficiary_user_id** | **int**| Credited wallet user&#39;s id. | [optional] 
 **transfer_date** | **datetime**| Transfert&#39;s date. Format : YYYY-MM-DD HH:MM:SS  | [optional] 
 **amount** | **str**| Transfert&#39;s amount. | [optional] 
 **currency** | **str**| Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | [optional] 
 **transfer_type_id** | **int**| The Type Id of the Transfer:  | ID | Description | |-----|-----| | 1 | Wallet to wallet | | 2 | Card transaction | | 3 | Client fees | | 4 | Credit note |  | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_transfers**
> object post_transfers(wallet_id, beneficiary_wallet_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transfer_tag=transfer_tag, label=label, transfer_type_id=transfer_type_id)

create a transfer

Create a new transfer in the system.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.TransferApi(swagger_client.ApiClient(configuration))
wallet_id = 56 # int | Debited wallet's ID
beneficiary_wallet_id = 56 # int | Credited wallet's ID
amount = 3.4 # float | Transfer's amount
currency = 'currency_example' # str | Transfert's currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
transfer_tag = 'transfer_tag_example' # str | Custom data. (optional)
label = 'label_example' # str | Custom data. (optional)
transfer_type_id = 56 # int | The Type Id of the Transfer:  | ID | Description | |-----|-----| | 1 | Wallet to wallet (default value) | | 3 | Client fees | | 4 | Credit note |  (optional)

try:
    # create a transfer
    api_response = api_instance.post_transfers(wallet_id, beneficiary_wallet_id, amount, currency, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, transfer_tag=transfer_tag, label=label, transfer_type_id=transfer_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TransferApi->post_transfers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **int**| Debited wallet&#39;s ID | 
 **beneficiary_wallet_id** | **int**| Credited wallet&#39;s ID | 
 **amount** | **float**| Transfer&#39;s amount | 
 **currency** | **str**| Transfert&#39;s currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **transfer_tag** | **str**| Custom data. | [optional] 
 **label** | **str**| Custom data. | [optional] 
 **transfer_type_id** | **int**| The Type Id of the Transfer:  | ID | Description | |-----|-----| | 1 | Wallet to wallet (default value) | | 3 | Client fees | | 4 | Credit note |  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


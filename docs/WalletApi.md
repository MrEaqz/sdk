# swagger_client.WalletApi

All URIs are relative to *https://sandbox.treezor.com/v1/index.php*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_wallet**](WalletApi.md#delete_wallet) | **DELETE** /wallets/{id} | delete a wallet
[**get_wallet**](WalletApi.md#get_wallet) | **GET** /wallets/{id} | get a wallet
[**get_wallets**](WalletApi.md#get_wallets) | **GET** /wallets | search wallets
[**post_wallets**](WalletApi.md#post_wallets) | **POST** /wallets | create a wallet
[**put_wallet**](WalletApi.md#put_wallet) | **PUT** /wallets/{id} | update a wallet


# **delete_wallet**
> object delete_wallet(id, origin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

delete a wallet

Change wallet's status to **CANCELED**.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.WalletApi(swagger_client.ApiClient(configuration))
id = 56 # int | Wallet's id
origin = 'origin_example' # str | Request's origin. Possible values are: * OPERATOR * USER 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # delete a wallet
    api_response = api_instance.delete_wallet(id, origin, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WalletApi->delete_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Wallet&#39;s id | 
 **origin** | **str**| Request&#39;s origin. Possible values are: * OPERATOR * USER  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_wallet**
> object get_wallet(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)

get a wallet



### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.WalletApi(swagger_client.ApiClient(configuration))
id = 789 # int | Object internal id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)

try:
    # get a wallet
    api_response = api_instance.get_wallet(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WalletApi->get_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Object internal id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_wallets**
> object get_wallets(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, wallet_id=wallet_id, wallet_status=wallet_status, user_id=user_id, parent_user_id=parent_user_id, wallet_tag=wallet_tag, wallet_type_id=wallet_type_id, event_alias=event_alias, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date, tariff_id=tariff_id, payin_count=payin_count, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)

search wallets

Get wallets from the system that match the search criteria. The request must contains at least one of those inputs : walletId, eventAlias, userId, walletTypeId

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.WalletApi(swagger_client.ApiClient(configuration))
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 56 # int | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
wallet_id = 56 # int | Wallet's unique id. (optional)
wallet_status = 'wallet_status_example' # str | Wallet's status:  * **validated**: Wallet is active. It is possible to deposit to or retrieve money from the wallet.  * **cancelled**: Wallet is closed. No action possible, there is no money in the wallet, it was closed by an operator or by the user.  * **pending**: Wallet being validated (e.g. suspected fraud, late submission of documents' validation ... KYC ). In this case, it is not possible to use the wallet for transactions. (optional)
user_id = 56 # int | Wallet owner's id. (optional)
parent_user_id = 56 # int | Parent user id of the wallet owner (optional)
wallet_tag = 'wallet_tag_example' # str | Custom data. (optional)
wallet_type_id = 56 # int | Wallet type id. The following values are defined:  | Id | Description | |----|----| | 9 | Electronic Money Wallet | | 10 | Payment Account Wallet | | 13 | Mirror Wallet | | 14 | Electronic Money Card (Internal only) |  (optional)
event_alias = 'event_alias_example' # str | Short url wallet name. It's automatically generated by the system, but it could be modified and customised by the user. For example: www.domain.com/wallet=mywedding  (optional)
event_payin_start_date = '2013-10-20T19:20:30+01:00' # datetime |  Wallet activation date. The date from which you can deposit money on the wallet. Format : YYYY-MM-DD HH:MM:SS (optional)
event_payin_end_date = '2013-10-20T19:20:30+01:00' # datetime | Deadline to money deposit on the wallet. Format : YYYY-MM-DD HH:MM:SS  (optional)
tariff_id = 56 # int | Wallet pricing id. (optional)
payin_count = 56 # int | Number of payin done on the wallet. (optional)
page_number = 56 # int | Pagination page number. More info [here](https://agent.treezor.com/lists).  (optional)
page_count = 56 # int | The number of items per page. More info [here](https://agent.treezor.com/lists).  (optional)
sort_by = 'sort_by_example' # str | The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  (optional)
sort_order = 'sort_order_example' # str | The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  (optional)
created_date_from = '2013-10-20T19:20:30+01:00' # datetime | The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
created_date_to = '2013-10-20T19:20:30+01:00' # datetime | The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_from = '2013-10-20T19:20:30+01:00' # datetime | The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)
updated_date_to = '2013-10-20T19:20:30+01:00' # datetime | The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  (optional)

try:
    # search wallets
    api_response = api_instance.get_wallets(access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, wallet_id=wallet_id, wallet_status=wallet_status, user_id=user_id, parent_user_id=parent_user_id, wallet_tag=wallet_tag, wallet_type_id=wallet_type_id, event_alias=event_alias, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date, tariff_id=tariff_id, payin_count=payin_count, page_number=page_number, page_count=page_count, sort_by=sort_by, sort_order=sort_order, created_date_from=created_date_from, created_date_to=created_date_to, updated_date_from=updated_date_from, updated_date_to=updated_date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WalletApi->get_wallets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **int**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **wallet_id** | **int**| Wallet&#39;s unique id. | [optional] 
 **wallet_status** | **str**| Wallet&#39;s status:  * **validated**: Wallet is active. It is possible to deposit to or retrieve money from the wallet.  * **cancelled**: Wallet is closed. No action possible, there is no money in the wallet, it was closed by an operator or by the user.  * **pending**: Wallet being validated (e.g. suspected fraud, late submission of documents&#39; validation ... KYC ). In this case, it is not possible to use the wallet for transactions. | [optional] 
 **user_id** | **int**| Wallet owner&#39;s id. | [optional] 
 **parent_user_id** | **int**| Parent user id of the wallet owner | [optional] 
 **wallet_tag** | **str**| Custom data. | [optional] 
 **wallet_type_id** | **int**| Wallet type id. The following values are defined:  | Id | Description | |----|----| | 9 | Electronic Money Wallet | | 10 | Payment Account Wallet | | 13 | Mirror Wallet | | 14 | Electronic Money Card (Internal only) |  | [optional] 
 **event_alias** | **str**| Short url wallet name. It&#39;s automatically generated by the system, but it could be modified and customised by the user. For example: www.domain.com/wallet&#x3D;mywedding  | [optional] 
 **event_payin_start_date** | **datetime**|  Wallet activation date. The date from which you can deposit money on the wallet. Format : YYYY-MM-DD HH:MM:SS | [optional] 
 **event_payin_end_date** | **datetime**| Deadline to money deposit on the wallet. Format : YYYY-MM-DD HH:MM:SS  | [optional] 
 **tariff_id** | **int**| Wallet pricing id. | [optional] 
 **payin_count** | **int**| Number of payin done on the wallet. | [optional] 
 **page_number** | **int**| Pagination page number. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **page_count** | **int**| The number of items per page. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_by** | **str**| The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **sort_order** | **str**| The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists).  | [optional] 
 **created_date_from** | **datetime**| The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **created_date_to** | **datetime**| The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_from** | **datetime**| The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 
 **updated_date_to** | **datetime**| The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists)  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_wallets**
> object post_wallets(wallet_type_id, tariff_id, user_id, currency, event_name, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, joint_user_id=joint_user_id, wallet_tag=wallet_tag, event_alias=event_alias, event_date=event_date, event_message=event_message, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date)

create a wallet

Create a new wallet in the system..

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.WalletApi(swagger_client.ApiClient(configuration))
wallet_type_id = 56 # int | Wallet type id. The following values are defined:  | Id | Description | |----|----| | 9 | Electronic Money Wallet | | 10 | Payment Account Wallet | | 13 | Mirror Wallet | | 14 | Electronic Money Card (Internal only) | 
tariff_id = 56 # int | Wallet pricing id.
user_id = 56 # int | Wallet owner's id.
currency = 'currency_example' # str | The currency that will be used for all wallet money transactions format : ISO 4217 3-letter code for each currency : Euro = EUR ; US Dollar = USD Ã¢â‚¬Â¦ Default currency will be same as the wallet 
event_name = 'event_name_example' # str | Event name that will be used as wallet name. For example, in a wedding list: Wedding of X and Y 
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 56 # int | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
joint_user_id = 56 # int | Wallet co-owner's id. This user will not be allowed to collect the money from the wallet.  (optional)
wallet_tag = 'wallet_tag_example' # str | Custom data (optional)
event_alias = 'event_alias_example' # str | Short url wallet name. It's automatically generated by the system, but it could be modified and customised by the user. For example: www.domain.com/wallet=mywedding. Please note that the eventAlias must be unique.  (optional)
event_date = '2013-10-20' # date | Wallet event date. Format : YYYY-MM-DD. Default: Created date + 7 days.  (optional)
event_message = 'event_message_example' # str | It can be a description of the wallet. (optional)
event_payin_start_date = '2013-10-20' # date |  DEPRECATED. Wallet activation date. The date from which you can deposit money on the wallet. Format : YYYY-MM-DD. If null, the default date will be the created date of the wallet. It will be deleted soon. (optional)
event_payin_end_date = '2013-10-20' # date | DEPRECATED. Deadline to money deposit on the wallet. Format : YYYY-MM-DD. If null, the money can be deposited indefinitely on the wallet. It will be deleted soon.  (optional)

try:
    # create a wallet
    api_response = api_instance.post_wallets(wallet_type_id, tariff_id, user_id, currency, event_name, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, joint_user_id=joint_user_id, wallet_tag=wallet_tag, event_alias=event_alias, event_date=event_date, event_message=event_message, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WalletApi->post_wallets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_type_id** | **int**| Wallet type id. The following values are defined:  | Id | Description | |----|----| | 9 | Electronic Money Wallet | | 10 | Payment Account Wallet | | 13 | Mirror Wallet | | 14 | Electronic Money Card (Internal only) |  | 
 **tariff_id** | **int**| Wallet pricing id. | 
 **user_id** | **int**| Wallet owner&#39;s id. | 
 **currency** | **str**| The currency that will be used for all wallet money transactions format : ISO 4217 3-letter code for each currency : Euro &#x3D; EUR ; US Dollar &#x3D; USD Ã¢â‚¬Â¦ Default currency will be same as the wallet  | 
 **event_name** | **str**| Event name that will be used as wallet name. For example, in a wedding list: Wedding of X and Y  | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **int**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **joint_user_id** | **int**| Wallet co-owner&#39;s id. This user will not be allowed to collect the money from the wallet.  | [optional] 
 **wallet_tag** | **str**| Custom data | [optional] 
 **event_alias** | **str**| Short url wallet name. It&#39;s automatically generated by the system, but it could be modified and customised by the user. For example: www.domain.com/wallet&#x3D;mywedding. Please note that the eventAlias must be unique.  | [optional] 
 **event_date** | **date**| Wallet event date. Format : YYYY-MM-DD. Default: Created date + 7 days.  | [optional] 
 **event_message** | **str**| It can be a description of the wallet. | [optional] 
 **event_payin_start_date** | **date**|  DEPRECATED. Wallet activation date. The date from which you can deposit money on the wallet. Format : YYYY-MM-DD. If null, the default date will be the created date of the wallet. It will be deleted soon. | [optional] 
 **event_payin_end_date** | **date**| DEPRECATED. Deadline to money deposit on the wallet. Format : YYYY-MM-DD. If null, the money can be deposited indefinitely on the wallet. It will be deleted soon.  | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_wallet**
> object put_wallet(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, wallet_type_id=wallet_type_id, event_name=event_name, event_alias=event_alias, event_date=event_date, event_message=event_message, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date, url_image=url_image, image_name=image_name, tariff_id=tariff_id)

update a wallet

Modifiy wallet information

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: api_key
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.WalletApi(swagger_client.ApiClient(configuration))
id = 789 # int | Object internal id.
access_signature = 'access_signature_example' # str | Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  (optional)
access_tag = 'access_tag_example' # str | Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_id = 56 # int | Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
access_user_ip = 'access_user_ip_example' # str | Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics).  (optional)
wallet_type_id = 'wallet_type_id_example' # str | Change wallet type id (optional)
event_name = 'event_name_example' # str | Change event name (optional)
event_alias = 'event_alias_example' # str | Change event alias. Please note that the eventAlias must be unique. (optional)
event_date = 'event_date_example' # str | Change event date (optional)
event_message = 'event_message_example' # str | Change event message (optional)
event_payin_start_date = 'event_payin_start_date_example' # str | DEPRECATED. Change event payin starting date. It will be deleted soon. (optional)
event_payin_end_date = 'event_payin_end_date_example' # str | DEPRECATED. Change event payin end date.  It will be deleted soon. (optional)
url_image = 'url_image_example' # str | Change URL image (optional)
image_name = 'image_name_example' # str | Change image name (optional)
tariff_id = 'tariff_id_example' # str | Change tariff id (optional)

try:
    # update a wallet
    api_response = api_instance.put_wallet(id, access_signature=access_signature, access_tag=access_tag, access_user_id=access_user_id, access_user_ip=access_user_ip, wallet_type_id=wallet_type_id, event_name=event_name, event_alias=event_alias, event_date=event_date, event_message=event_message, event_payin_start_date=event_payin_start_date, event_payin_end_date=event_payin_end_date, url_image=url_image, image_name=image_name, tariff_id=tariff_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WalletApi->put_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Object internal id. | 
 **access_signature** | **str**| Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication).  | [optional] 
 **access_tag** | **str**| Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_id** | **int**| Access user&#39;s id is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **access_user_ip** | **str**| Access user&#39;s ip is used for user&#39;s action restriction. More info [here](https://agent.treezor.com/basics).  | [optional] 
 **wallet_type_id** | **str**| Change wallet type id | [optional] 
 **event_name** | **str**| Change event name | [optional] 
 **event_alias** | **str**| Change event alias. Please note that the eventAlias must be unique. | [optional] 
 **event_date** | **str**| Change event date | [optional] 
 **event_message** | **str**| Change event message | [optional] 
 **event_payin_start_date** | **str**| DEPRECATED. Change event payin starting date. It will be deleted soon. | [optional] 
 **event_payin_end_date** | **str**| DEPRECATED. Change event payin end date.  It will be deleted soon. | [optional] 
 **url_image** | **str**| Change URL image | [optional] 
 **image_name** | **str**| Change image name | [optional] 
 **tariff_id** | **str**| Change tariff id | [optional] 

### Return type

**object**

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


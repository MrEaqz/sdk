# coding: utf-8

"""
    Treezor

    Treezor API.  more info [here](https://www.treezor.com).   # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class TransferrefundApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_transferrefund(self, id, **kwargs):  # noqa: E501
        """cancel a transfer refund  # noqa: E501

        Change transfer refund's status to CANCELED. A validated transfer refund can't be cancelled.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_transferrefund(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Transferrefunds internal id. (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.delete_transferrefund_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.delete_transferrefund_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def delete_transferrefund_with_http_info(self, id, **kwargs):  # noqa: E501
        """cancel a transfer refund  # noqa: E501

        Change transfer refund's status to CANCELED. A validated transfer refund can't be cancelled.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_transferrefund_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Transferrefunds internal id. (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_transferrefund" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if self.api_client.client_side_validation and ('id' not in params or
                                                       params['id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `id` when calling `delete_transferrefund`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/transferrefunds/{id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_transferrefund(self, id, **kwargs):  # noqa: E501
        """get a transfer refund  # noqa: E501

        Get a transfer refund from the system.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_transferrefund(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Trasnfert refund internal id. (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_transferrefund_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_transferrefund_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def get_transferrefund_with_http_info(self, id, **kwargs):  # noqa: E501
        """get a transfer refund  # noqa: E501

        Get a transfer refund from the system.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_transferrefund_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: Trasnfert refund internal id. (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_transferrefund" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if self.api_client.client_side_validation and ('id' not in params or
                                                       params['id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `id` when calling `get_transferrefund`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/transferrefunds/{id}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_transferrefunds(self, **kwargs):  # noqa: E501
        """search transfer refunds  # noqa: E501

        Get transfer refunds that match search criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_transferrefunds(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param int transferrefund_id: Transfer refund id.
        :param str transferrefund_tag: Custom data.
        :param str transferrefund_status: Transfer refund status. Possible values: * PENDING * CANCELED * VALIDATED 
        :param int wallet_id: Refunded wallet's id.
        :param int transfer_id: Initial transfer's id.
        :param int transferrefund_date: transfert refund's date. Format: YYYY-MM-DD HH:MM:SS 
        :param int user_id: User's id of who has made the transfer refund.
        :param str amount: Refund amount.
        :param str currency: Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). 
        :param int page_number: Pagination page number. More info [here](https://agent.treezor.com/lists). 
        :param int page_count: The number of items per page. More info [here](https://agent.treezor.com/lists). 
        :param str sort_by: The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists). 
        :param str sort_order: The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists). 
        :param datetime created_date_from: The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime created_date_to: The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime updated_date_from: The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime updated_date_to: The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_transferrefunds_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_transferrefunds_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_transferrefunds_with_http_info(self, **kwargs):  # noqa: E501
        """search transfer refunds  # noqa: E501

        Get transfer refunds that match search criteria.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_transferrefunds_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param int transferrefund_id: Transfer refund id.
        :param str transferrefund_tag: Custom data.
        :param str transferrefund_status: Transfer refund status. Possible values: * PENDING * CANCELED * VALIDATED 
        :param int wallet_id: Refunded wallet's id.
        :param int transfer_id: Initial transfer's id.
        :param int transferrefund_date: transfert refund's date. Format: YYYY-MM-DD HH:MM:SS 
        :param int user_id: User's id of who has made the transfer refund.
        :param str amount: Refund amount.
        :param str currency: Transfert amount currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217). 
        :param int page_number: Pagination page number. More info [here](https://agent.treezor.com/lists). 
        :param int page_count: The number of items per page. More info [here](https://agent.treezor.com/lists). 
        :param str sort_by: The transaction element you want to sort the list with. Default value : _createdDate_. More info [here](https://agent.treezor.com/lists). 
        :param str sort_order: The order you want to sort the list. * **DESC** for a descending sort * **ASC** for a ascending sort.  Default value : DESC. More info [here](https://agent.treezor.com/lists). 
        :param datetime created_date_from: The creation date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime created_date_to: The creation date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime updated_date_from: The modification date from which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :param datetime updated_date_to: The modification date up to which you want to filter the request result. Format YYYY-MM-DD HH:MM:SS. More info [here](https://agent.treezor.com/lists) 
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['access_signature', 'access_tag', 'access_user_id', 'access_user_ip', 'transferrefund_id', 'transferrefund_tag', 'transferrefund_status', 'wallet_id', 'transfer_id', 'transferrefund_date', 'user_id', 'amount', 'currency', 'page_number', 'page_count', 'sort_by', 'sort_order', 'created_date_from', 'created_date_to', 'updated_date_from', 'updated_date_to']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_transferrefunds" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_signature' in params:
            query_params.append(('accessSignature', params['access_signature']))  # noqa: E501
        if 'access_tag' in params:
            query_params.append(('accessTag', params['access_tag']))  # noqa: E501
        if 'access_user_id' in params:
            query_params.append(('accessUserId', params['access_user_id']))  # noqa: E501
        if 'access_user_ip' in params:
            query_params.append(('accessUserIp', params['access_user_ip']))  # noqa: E501
        if 'transferrefund_id' in params:
            query_params.append(('transferrefundId', params['transferrefund_id']))  # noqa: E501
        if 'transferrefund_tag' in params:
            query_params.append(('transferrefundTag', params['transferrefund_tag']))  # noqa: E501
        if 'transferrefund_status' in params:
            query_params.append(('transferrefundStatus', params['transferrefund_status']))  # noqa: E501
        if 'wallet_id' in params:
            query_params.append(('walletId', params['wallet_id']))  # noqa: E501
        if 'transfer_id' in params:
            query_params.append(('transferId', params['transfer_id']))  # noqa: E501
        if 'transferrefund_date' in params:
            query_params.append(('transferrefundDate', params['transferrefund_date']))  # noqa: E501
        if 'user_id' in params:
            query_params.append(('userId', params['user_id']))  # noqa: E501
        if 'amount' in params:
            query_params.append(('amount', params['amount']))  # noqa: E501
        if 'currency' in params:
            query_params.append(('currency', params['currency']))  # noqa: E501
        if 'page_number' in params:
            query_params.append(('pageNumber', params['page_number']))  # noqa: E501
        if 'page_count' in params:
            query_params.append(('pageCount', params['page_count']))  # noqa: E501
        if 'sort_by' in params:
            query_params.append(('sortBy', params['sort_by']))  # noqa: E501
        if 'sort_order' in params:
            query_params.append(('sortOrder', params['sort_order']))  # noqa: E501
        if 'created_date_from' in params:
            query_params.append(('createdDateFrom', params['created_date_from']))  # noqa: E501
        if 'created_date_to' in params:
            query_params.append(('createdDateTo', params['created_date_to']))  # noqa: E501
        if 'updated_date_from' in params:
            query_params.append(('updatedDateFrom', params['updated_date_from']))  # noqa: E501
        if 'updated_date_to' in params:
            query_params.append(('updatedDateTo', params['updated_date_to']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/transferrefunds', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def post_transferrefunds(self, transfer_id, amount, currency, **kwargs):  # noqa: E501
        """create a transfer refund  # noqa: E501

        Create a new transfer refund in the system.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_transferrefunds(transfer_id, amount, currency, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int transfer_id: transfer's id to refund. (required)
        :param float amount: Refund amount (required)
        :param str currency: Transfert's currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (required)
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str transferrefund_tag: Custom data.
        :param str comment: End user, client or issuer comment.
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.post_transferrefunds_with_http_info(transfer_id, amount, currency, **kwargs)  # noqa: E501
        else:
            (data) = self.post_transferrefunds_with_http_info(transfer_id, amount, currency, **kwargs)  # noqa: E501
            return data

    def post_transferrefunds_with_http_info(self, transfer_id, amount, currency, **kwargs):  # noqa: E501
        """create a transfer refund  # noqa: E501

        Create a new transfer refund in the system.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_transferrefunds_with_http_info(transfer_id, amount, currency, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int transfer_id: transfer's id to refund. (required)
        :param float amount: Refund amount (required)
        :param str currency: Transfert's currency. Debited wallet and credited wallet must share same currency. Format: [ISO 4217](https://fr.wikipedia.org/wiki/ISO_4217).  (required)
        :param str access_signature: Access signature can be mandatory for specific context. Treezor will contact you if so. More info [here](https://agent.treezor.com/security-authentication). 
        :param str access_tag: Access tag is used for idem potency query. More info [here](https://agent.treezor.com/basics). 
        :param int access_user_id: Access user's id is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str access_user_ip: Access user's ip is used for user's action restriction. More info [here](https://agent.treezor.com/basics). 
        :param str transferrefund_tag: Custom data.
        :param str comment: End user, client or issuer comment.
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['transfer_id', 'amount', 'currency', 'access_signature', 'access_tag', 'access_user_id', 'access_user_ip', 'transferrefund_tag', 'comment']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_transferrefunds" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'transfer_id' is set
        if self.api_client.client_side_validation and ('transfer_id' not in params or
                                                       params['transfer_id'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `transfer_id` when calling `post_transferrefunds`")  # noqa: E501
        # verify the required parameter 'amount' is set
        if self.api_client.client_side_validation and ('amount' not in params or
                                                       params['amount'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `amount` when calling `post_transferrefunds`")  # noqa: E501
        # verify the required parameter 'currency' is set
        if self.api_client.client_side_validation and ('currency' not in params or
                                                       params['currency'] is None):  # noqa: E501
            raise ValueError("Missing the required parameter `currency` when calling `post_transferrefunds`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'access_signature' in params:
            query_params.append(('accessSignature', params['access_signature']))  # noqa: E501
        if 'access_tag' in params:
            query_params.append(('accessTag', params['access_tag']))  # noqa: E501
        if 'access_user_id' in params:
            query_params.append(('accessUserId', params['access_user_id']))  # noqa: E501
        if 'access_user_ip' in params:
            query_params.append(('accessUserIp', params['access_user_ip']))  # noqa: E501
        if 'transferrefund_tag' in params:
            query_params.append(('transferrefundTag', params['transferrefund_tag']))  # noqa: E501
        if 'transfer_id' in params:
            query_params.append(('transferId', params['transfer_id']))  # noqa: E501
        if 'comment' in params:
            query_params.append(('comment', params['comment']))  # noqa: E501
        if 'amount' in params:
            query_params.append(('amount', params['amount']))  # noqa: E501
        if 'currency' in params:
            query_params.append(('currency', params['currency']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['api_key']  # noqa: E501

        return self.api_client.call_api(
            '/transferrefunds', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

# coding: utf-8

"""
    Treezor

    Treezor API.  more info [here](https://www.treezor.com).   # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.beneficiaries_api import BeneficiariesApi  # noqa: E501
from swagger_client.rest import ApiException


class TestBeneficiariesApi(unittest.TestCase):
    """BeneficiariesApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.beneficiaries_api.BeneficiariesApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_beneficiaries(self):
        """Test case for get_beneficiaries

        search beneficiary bank accounts  # noqa: E501
        """
        pass

    def test_get_beneficiary(self):
        """Test case for get_beneficiary

        get a beneficiary bank account  # noqa: E501
        """
        pass

    def test_post_beneficiary(self):
        """Test case for post_beneficiary

        create a beneficiary  # noqa: E501
        """
        pass

    def test_put_beneficiary(self):
        """Test case for put_beneficiary

        edit a beneficiary  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
